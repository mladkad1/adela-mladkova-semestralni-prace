import os

winning_dialogue = 68

class Dialogue:
    def __init__(self, text, options, events = [], skills = {}):
        self.text = text
        self.options = options
        self.events = events
        self.skills = skills 
        self.skill_applied = False
    
    def display_text(self):
        print(self.text)

    def display_options(self):
        if (self.options == []):
            return

        print("")
        for i, (key, value) in enumerate(self.options.items()):
                    print(f"{i + 1}. {key}")
    
    def apply_skills(self, player):
        if not self.skill_applied: 
            for skill, level in self.skills.items():
                if skill in player.skills:
                    previous_level = player.skills[skill]
                    player.skills[skill] += level
                    if player.skills[skill] > previous_level:
                        print(f"Tvoje {skill} se zvýšila o +1!")
                else: 
                    player.skills[skill] = level 
            self.skill_applied = True

class Event:
    def __init__(self, text, type, amount):
        self.text = text
        self.type = type
        self.amount = amount 

    def display(self):
        print("\n" + f"*{self.text}*")

class Player:
    def __init__(self):
        self.name = "Jonáš Bořivoj Harvát"
        self.health = 100
        self.inventory = []
        self.skills = {
            "Síla": 1,
            "Rychlost": 1,
            "Štěstí": 1
        }

    def lose_health(self, amount):
        self.health -= amount

    def earn_points(self, amount):
        self.health += amount    
    
    def heal(self, amount):
        self.health += amount

    def is_alive(self):
        return self.health > 0

    def display_stats(self):
        for i in range(50):
            print("-", end="")

        print("\nJonáš Bořivoj Harvát")
        print(f"Bodíky zdraví:\t{self.health} HP")
        print(f"\nInventář: {','.join(self.inventory)}")
        print("\nSchopnosti:")
        for skill, level in self.skills.items():
            print(f"{skill}: Level {level}")

        for i in range(50):
            print("-", end="")
        print("\n")

    def add_to_inventory(self, item):
        self.inventory.append(item)
    
    def remove_from_inventory(self, item):
        if item in self.inventory:
            self.inventory.remove(item)

class Game:
    def __init__(self):
        self.player = Player()
        self.dialogue = [
            Dialogue("Vítej, Jonáši Bořivoji Harváte.\nNacházíš se uprostřed temného lesa. Zabloudil jsi?", {"Ano, chci pryč.": 1, "Ne. Vydal jsem se ze spárů špinavého draka zachránit spanilou princeznu Lilien, \n  o jejíž kráse se v našem království pějí písně.": 2}), 
            Dialogue("Cestu ven z lesa najdeš, pokud u každého pařezu zahneš doleva.", []),
            Dialogue("Skvěle! Princezna je uvězněná ve věži, ke které musíš najít cestu strastiplným terénem a nakonec se utkat s drakem Rumbudrakem.", {"Jdu na to!": 3, "Mám strach..": 1}),
            Dialogue("Vydal ses na cestu, ale kousl tě do nohy jedovatý had.", {":(": 4, "Auuu, to bolí!": 4}, 
                     [Event("Ztrácíš 10 HP.", "damage", 10)]),
            Dialogue("To nevadí. Cesta pravého hrdiny zahrnuje i překážky ve formě nepatřičných nehod,\nnáhod, různorodých nestvůr, nebezpečenství, lásky, zrady, slz a potu.\nJsi připraven? Máš na to? Cítíš se jako někdo, kdo je opravdu hoden takové\nvýzvy, jako je záchrana líbezné princezny ze spárů zrůdné bestie?\nNa dodání odvahy ti lesní božstva seslaly meč.", {"Ano! Cítím se být hrdinou!": 5, "*krátká odmlka* Ano. Pro princeznu Lilien udělám vše, co můžu.": 5}, 
                     [Event("Získal si meč.", "add_to_inventory", "meč")]),
            Dialogue("Fajn. Je čas se předvést.\nPomalu se stmívá. Běžíš se schovat do blízké jeskyně, nebo i po tmě najdeš cestu z lesa,\nkterá tě přiblíží věži s uvězněnou princeznou?", {"Jdu se schovat, les v noci je nebezpečné místo...": 6,"Jdu dál, princezna na mě čeká!": 7}),
            Dialogue("Bohužel. V jeskyni se schovávala obří tarantule, která tě spoutala a vysála z tebe život.", [], 
                     [Event("Umřel jsi.", "damage", 90)]),
            Dialogue("Dobře. Les je ale nehostinné místo, a ve tmě se může stát přímo osudným. Je potřeba mít ochranu. Volíš si nůž, nebo lucernu?", {"Volím si nůž.": 8, "Volím si lucernu.": 12}), 
            Dialogue("Zvolil sis nůž. Nůž máš možnost nalézt, nebo o něj poprosit lesní božstva.", {"Nůž zkusím nalézt.": 9, "Zazpívám lesním bohům.": 11}),
            Dialogue("Nůž nalezneš použitím magnetu, který máš v kapse.", {"Dám se do hledání.": 10, "To nemůže fungovat.": 1}),
            Dialogue("Důvěra se vyplatila. Nalezl si krásný nůž z damascénské oceli. Je čas se vydat na cestu.\nStarci z tvé vesnice říkávali, že cestu směrem k věži člověk nalezne, pokud půjde stále směrem za nosem.", {"Jdu rovně za nosem.": 15 , "Jdu na opačnou stranu.": 1}, 
                     [Event("Získal si nůž.", "add_to_inventory", "nůž")]),
            Dialogue("Blíží se noc a ty ses rozhodl zpívat, zelenáči?", [], 
                     [Event("Umřel jsi.", "damage", 100)]),
            Dialogue("Zvolil sis lucernu. Bojíš se, že dojde plyn a budeš raději následovat světlušky, nebo lucerně věříš?", {"Věřím přírodním silám.": 13, "Věřím tomu, že mě lucernička nezklame.": 14}, 
                     [Event("Získal si lucernu.", "add_to_inventory", "lucerna")]),
            Dialogue("Bylo by lepší věřit něčemu jinému, než tvorům se shlukem buněk místo mozku.", [], 
                     [Event("Světlušky tě dovedly k bezednému jezeru, do kterého si spadl a utopil se.", "damage", 90)]),
            Dialogue("Máš pod čepicí. Je čas se vydat na cestu.\nStarci z tvé vesnice říkávali, že cestu směrem k věži člověk nalezne, pokud půjde stále směrem za nosem.", {"Jdu rovně za nosem.": 15, "Jdu na opačnou stranu.": 1}),
            Dialogue("Jdeš už dlouho bez zastavení, skvěle! Lesní bohové oceňují tvoji chrabrost, bystrost a píli a odměnili tě.\nU tamtoho velkého velkého stromu zahneš podél mechu na kmeni, nebo proti?", {"Jdu podél mechu.": 16, "Jdu proti směru mechu.": 16}, 
                     [Event("Dostal si HP navíc.", "earn_points", 10)]),
            Dialogue("Na mlhavém obzoru vidíš se mihotat jakési obrysy, možná lidské.. možná zvířecí. Pokračuješ dál?", {"Ano! Jsem pro záchranu princezny Lilien ochoten udělat cokoliv.": 17, "No, zní to nebezpečně.": 1}),
            Dialogue("Neohroženě jdeš vpřed. Podél průzračného potůčka, na velkém kameni, sedí krásná dívka. Jdeš blíž k ní?", {"Samozřejmě! Možná je také v nesnázích, stejně jako Lilien..": 18, "Nejsem žádný Honza, jdu blíž, ale raději si připravím nůž.": 18}),
            Dialogue("Dívka na tebe kouká velkými blankytnými oči. Jemným hlasem tě vybízí, abys jí políbil. Políbíš ji?", {"Cesta je dlouhá a mě chybí dívčí rty. Dívku políbím...": 19, "Vím, že cesta hrdiny je provázena nebezpečími, raději se jen usměju a odejdu.": 20}),
            Dialogue("Sice ses neproměnil v žabáka, ale polila tě vina. Doma na tebe čeká tvoje Maruška. Vydáš se dál?", {"Ano.": 21, "Ne. Jdu zpět za Maruškou.": 1}, 
                     [Event("Za líbání cizích dívek ztrácíš 40 bodů.", "damage", 40)]),
            Dialogue("Jsi sice čestný, ale udeřil na tebe ženský hněv. Vydáš se dál?", {"Ano.": 21, "Ne. Jdu zpět za Maruškou.": 1}, 
                     [Event("Za nesplnění ženského přání ztrácíš 20 bodů.", "damage", 20)]),
            Dialogue("To byl ale divný zážitek, co? Kdo ví, co tě ale čeká dál... Tamhle je cesta, půjdeš po ní?", {"Ano.": 22, "Ne. Půjdu dál podél potoka.": 23}), 


            Dialogue("Cesta tě vyvedla ven z lesa. Jdeš již dlouho, a narazil si na vesnici Stříbrné Hlubiny. Zdá se, že okolo jít nemůžeš, protože se nachází v rokli mezi skalami.\nJsi unavený. Ve vesnici se nachází hospoda i hostinec s přespáním.", {"Tikají hodiny, pouze se posilním polévkou. Najdu hospodu.": 24, "Jsem unavený, a vyspaný budu silnější. Najdu hostinec.": 25}), 
            Dialogue("Potok je zakletý a ztratil ses.", []),
            Dialogue("Hospodský uvařil pořádnou šlichtu! Dostáváš body navíc.\nV hospodě tě oslovil jakýsi nuzák. Dáš se s ním do řeči?", {"Samozřejmě.": 26, "Raději ne.": 27}, 
                     [Event("Dostal si 20 bodů k dobru.", "heal", 20)]),  
            Dialogue("V hostinci ses výborně najedl a zregeneroval síly.\nBuclaté hostinské ses líbil. Prozradila ti, že v domě je schovaný nevěstinec. Zavítáš do něj?", {"Nevěstinec si nenechám ujít.": 28, "Raději ne.": 27}, 
                     [Event("Dostal si 30 bodů k dobru.", "heal", 30)]),
            Dialogue("Nuzák se tě chvíli vyptával. Pak tě pozval k němu, že si od něj můžeš koupit koně.\nVypadal podezřele, ale žádná bota v tom nejspíš není. Vybereš si šedáka, nebo hnědáka?", {"Vybírám si šedáka.": 29, "Vybírám si hnědáka.": 29}),
            Dialogue("Škoda. Mohlo z toho něco zajímavého kápnout. Pokračuješ dál na východ, nebo na západ?", {"Na východ.": 30, "Na západ.": 30}),
            Dialogue("Dívky si vyslechly tvůj příběh o záchraně princezny. Líbí se jim tvoje odvaha, a proto tě odměnily kouzelným lekvarem, který ti dodá sílu. Pokračuješ dál na východ, nebo na západ?", {"Na východ.": 30, "Na západ.": 30}, 
                     [Event("Získal si lektvar.", "add_to_inventory", "lektvar")], skills = {"Síla": 1}), 
            Dialogue("Dostal si nového kamaráda, s kterým budeš na cestě o to rychlejší! Pokračuješ dál na východ, nebo na západ?", {"Na východ.": 30, "Na západ.": 30}, 
                     [Event("Získal si koně.", "add_to_inventory", "kůň")], skills = {"Rychlost": 1}), 
            Dialogue("Jsi opět na cestě. Ale ale! V dálce stojí okruh lidí okolo ukřičeného kejklíře.\nJdeš blíž. Kejklíř si tě všiml a nabídl ti zahrát si s ním hru. Přijmeš nabídku?", {"Přijmu, třeba se přihodí něco zajímavého.": 31, "Nepřijmu, princezna čeká jen a jen na mně.": 32}),
            Dialogue("*Ahoj panáčku! Zahrajeme si hru o štěstí. Zvolíš si pravou, nebo levou ruku?", {"Pravou.": 33, "Levou.": 34}),
            Dialogue("Fajn. Ve vesnici si tě vyhlídla ještě jedna osůbka, podezřelý hezoun. Dáte se do řeči.\nZjišťuješ, že také zachraňuje princeznu Lilien. Jako správní muži se utkáte v zápase.\n*Jestli, Jonáši Bořivoji Harváte, uhádneš-li moji hádanku, nechám tě mě zabít. Jestli neuhádneš, zabiju já tebe. Souhlasíš?*", {"Platí, utkám se s tebou!": 35, "Platí.": 35}),
            Dialogue("Výborně, zvolil si šťastnou ruku. Jdeš dál?", {"Ano.": 32, "Jdu!": 32}, skills = {"Štěstí": 1}),
            Dialogue("To nebyla šťastná volba. Kejklíř se začal tak moc smát, až se mu tak roztáhla pusa, že tě sežral a ty si umřel.", []),
            Dialogue("*Teď pozorně poslouchej.\nV středu města stojí bez hradby,\nvýš se tyčí než kostel.\nBez nohou chodí, bez očí vidí,\na přesto všechno stále kráčí vpřed.\nCo to je?*", {"Hmm... To je těžké. Mou odpovědí je 'den'.": 37, "Hmm... To je těžké. Mou odpovědí je 'vodovodní věž'.": 36}),
            Dialogue("To nebyla správná odpověď. Teď tě zabijuuUUUUUUU!!!!!!", []),
            Dialogue(":O Odpověděl si správně....\nkchhhrkkkkkchhhhhhhh..........", {"To byla ale špinavá práce.": 38, "Dobře mu tak!": 38}),


            Dialogue("To byla teda divná vesnice, co? Raději si trochu odpočinout. Budeš támhle pod tím stromem spát celou noc, nebo celou noc a den?", {"Celou noc, princezna nepočká.": 39, "Celou noc a den, nevím, kdy jsem naposledy spal.": 40}),
            Dialogue("Vyspal ses pořádně. To bodlo!\nV dálce vidíš stařenku, která se lopotí s těžkou nůší. Pomůžeš jí?", {"Samozřejmě, to je údělem hrdiny!": 41, "Samozřejmě, že stařence pomůžu!": 41}, 
                     [Event("Dostáváš 30 HP navíc.", "heal", 30)]),
            Dialogue("Vyspal ses pořádně. To bodlo!\nV dálce vidíš stařenku, která se lopotí s těžkou nůší. Pomůžeš jí?", {"Samozřejmě, to je údělem hrdiny!": 41, "Samozřejmě, že stařence pomůžu!": 41}, 
                     [Event("Dostáváš 40 HP navíc.", "heal", 40)]),
            Dialogue("To jsi hodný. Stařenka se ukázala jako kouzelná, a za tvoji nápomocnost tě odměnila. Chceš být v závěrečném boji s drakem rychlejší, nebo silnější?", {"Chci být rychlejší.": 42, "Chci sílu.": 43}),
            Dialogue("Vybral sis rychlost. Pokračuješ ve své cestě za Lilien dál?", {"Ano!": 44, "Ne:/ už mě to nebaví.": 45}, skills = {"Rychlost": 1}),
            Dialogue("Vybral sis sílu. Pokračuješ ve své cestě za Lilien dál?", {"Ano!": 44, "Ne:/ už mě to nebaví.": 45}, skills = {"Síla": 1}),
            Dialogue("Jsi statečný. Půjdeš po prašné, nebo kamenité cestě?", {"Po prašné.": 46, "Po kamenité.": 47}),
            Dialogue("To mě mrzí.", []),
            Dialogue("Ta je prašná dost! Prach se ti dostal do očí. Pokračuješ?", {"Samozřejmě.": 48, "Ano.": 48}, 
                     [Event("Ztrácíš 10 HP.", "damage", 10)]),
            Dialogue("Po kamenech se špatně chodí. Pokračuješ?", {"Ano.": 48, "Samozřejmě.": 48}, 
                     [Event("Ztrácíš 10 HP.", "damage", 10)]),
            Dialogue("Tamhle v dálce vidíš snad nějaké dítě.\nJdeš se podívat blíže? Malé děti tady nemají co dělat!", {"To je podezřelé, jdu to prozkoumat.": 49, "Nesmím se už zdržovat...": 50}),
            Dialogue("To není dítě, to musí být trpaslík! Trpaslíci už měli být dávno vyhynutí.\nPřistoupíš k němu, nebo ti to přijde podezřelé a raději si budeš hledět svého?", {"Půjdu blíž.": 51, "Budu si hledět svého.": 50}),
            Dialogue("Back on track. Jdeš už dlouho, už na sobě cítíš známky vyčerpání..", {"Achjo.": 57, ":((": 57}, 
                     [Event("Ztrácíš 10 HP", "damage", 15)]), 
            Dialogue("*Nazdar tuláku. Co tu děláš?*", {"Co tu děláš ty? V životě jsem neviděl trpaslíka.": 52, "Hledím si svýho obludo.": 50}), 
            Dialogue("To víš. Tento kraj je plný různorodých magických bytostí. Já jsem ale hodný. Fakt.\nProcvičím tě v boji, chceš? Vlaštovky mi vyprávěly, že se chystáš na záchranu princezny Lilien ze spárů hnusného draka Rumbudraka.", {"*Jak to ví?* No, rád se od tebe něco přiučím.": 53, "*To zní divně, mám trochu strach.* No, zkusím to..": 53}),
            Dialogue("Jsi udatný a nebojíš se. To se mi líbí. Za odměnu ti daruju kopí.", {"To jsem nečekal.": 54, "Jsi opravdu hodný!": 54}, 
                     [Event("Získal si kopí.", "add_to_inventory", "kopí")]),
            Dialogue("Tak a teď k boji. Buď ostražitý. Neublížím ti. UaaaaGhhhHHHHHRrrrrrr!!!!!!", {"Aaaaaaa!!!!!!!": 55, "HrrrrRRRRR!!!!!": 56}),
            Dialogue("To bylo dobré, co? Uf, jsem celý zapocený.. Umíš se rvát, to je dobře. Hodně štěstí na tvé další cestě!", {"Trpaslíky mám rád!": 50, "To byl ale boj.. cítím se připraven utkat se s Rumbudrakem!": 50}, 
                     [Event("Dostáváš 35 HP navíc.", "heal", 35)], skills = {"Rychlost": 1}),
            Dialogue("To bylo dobré, co? Uf, jsem celý zapocený.. Umíš se rvát, to je dobře. Hodně štěstí na tvé další cestě!", {"Trpaslíky mám rád!": 50, "To byl ale boj.. cítím se připraven utkat se s Rumbudrakem!": 50}, 
                     [Event("Dostáváš 35 HP navíc.", "heal", 35)], skills = {"Síla": 1}),
            Dialogue("POZOR!!! Blíží se k tobě strašlivá obluda s pěti hlavami a třemi páry křídel!", {"Já se ničeho nezaleknu, budu bojovat!": 58, "Co mám dělat? Proti takové šeredě nemám šanci!": 59}),
            Dialogue("Trvalo to celou noc, a přišel si o malíček, ale obludu se ti povedlo zneškodnit. Teď už nebude nikoho obtěžovat.\nJsi skutečný hrdina.", {"Díky!": 60, "No stálo to spoustu sil!": 60}, 
                     [Event("Ztrácíš 50 HP.", "damage", 35)], skills = {"Štěstí": 1}),
            Dialogue("Obluda tě roztrhala, spolykala vnitřnosti a naočkovala jedem, který tě udržel při životě a donutil se dívat.", []),
            Dialogue("Samá překvapení. Jen ses stačil rozkoukat po téměř fatálním setkání s příšerou, hned vidíš něco dalšího.\nJablko. Máš ukrutný hlad, a tak zahodíš zábrany a zakousneš se. Dáš si červené, nebo zelené?", {"Dám si červené, to bude šťavnatější.": 61, "Raději si dám zelené, červená je barva zrady.": 62}),
            Dialogue("To nebylo štastné rozhodnutí.\nV červeném jablce byla ukrytá zmije, která tě kousla a tys velmi pomalu a bolestivě zemřel.", []),
            Dialogue("Nejsi hloupý, Jonáši. V červeném jablku se ukrývala zmije, který by tě byla zabila.\nZdá se, že se blížíš k věži s uvězněnou princeznou, je již vidět za obzorem.", {"Super!": 63, "Konečně!": 63}, 
                     [Event("Jablko ti dodalo sílu.", "heal", 5)]),


            Dialogue("Vypadá, že cesta se blíží ke konci. Na přidání tolik potřebné důvtipnosti, ti nebesa zahrají písničku. Klikni.", {"https://www.youtube.com/watch?v=dQw4w9WgXcQ": 64, "https://www.youtube.com/watch?v=yyDUC1LUXSU": 64}),
            Dialogue("To bylo povzbuzení, co?! Je čas se setkat s drakem. Jsi připravený?", {"ANO!": 65, "Ano!": 65}),
            Dialogue("Hned co tě zahlédl, obrovský drak Rumbudrak se odrazil od věže a seslal se k zemi směrem k tobě.\nUštědřil ti první velkou ránu!", {"AU! Ty přerostlá ještěrko!": 66, "AU! Jen počkej!": 66}),
            Dialogue("Rozzuřil ses do běla! Tomu drakovi musíš ukázat zač je toho loket!", {"Probodnu mu srdce!": 67, "Useknu mu hlavu!": 68}),
            Dialogue("Drak má olověné srdce.. Došel si tak daleko, chybělo tak málo....", []),
            Dialogue("Drak naposledy zachroptěl a skácel se k zemi.. teď jen vylézt nahoru do věže a políbit nádhernou princeznu. Hurá na líbánky!", {"<3": 69, "<33": 69}),

        ]

        self.current_dialogue = 0

    def play(self):
        # dialogue index is out of range of array
        if (self.current_dialogue > len(self.dialogue) - 1):
            print("Konec hry.")
            return

        # set current dialogue from array based on index
        dialogue = self.dialogue[self.current_dialogue]

        if dialogue.skills:
            dialogue.apply_skills(self.player)

        # realize events
        for event in dialogue.events:
            if event.type == "damage":
                self.player.lose_health(event.amount)
            elif event.type == "earn_points":
                self.player.earn_points(event.amount)
            elif event.type == "heal":
                self.player.heal(event.amount)
            elif event.type == "add_to_inventory":
                self.player.add_to_inventory(event.amount)

        self.player.display_stats()

        # display dialogue text
        dialogue.display_text()

        # display event text
        for event in dialogue.events:
            event.display()

        # print skill increase message
        if dialogue.skill_applied:
            for skill, level in dialogue.skills.items():
                if level > 0:
                    print(f"Tvoje {skill} se zvýšila o +{level}!")

        # if player is dead, end the game
        if not self.player.is_alive():
            print("*Nic se neděje. Zkus to znovu.*")
            return

        # if there are no options, end the game
        if dialogue.options == []:
            print("*Konec hry.*")
            return

        # winning conditions
        if self.current_dialogue >= winning_dialogue:
            if len(self.player.inventory) >= 3 and sum(level >= 2 for level in self.player.skills.values()) >= 2:
                print("Gratuluji! Vyhrál jsi!")
            else:
                print("Je mi to líto. Prohrál si.")
                return

        # display options for progressing dialogue
        dialogue.display_options()

        # next dialogue selection loop: runs until valid choice is made
        while True:
            choice = input("\nVyber číslo možnosti >")

            # check is input is valid: is a number and is in range of options
            if not choice.isdigit() or int(choice) < 1 or int(choice) > len(dialogue.options):
                print(">Neplatná volba.")
                # jump back to the start of the loop
                continue
            break
        
        # set next dialogue based on player choice
        next_dialogue_index = list(dialogue.options.keys())[int(choice) - 1]
        self.current_dialogue = dialogue.options[next_dialogue_index]

        os.system('cls')
        self.play()

game = Game()
os.system('cls')
game.play()


# mohl by být lépe ošetřený konec hry (exit při posledním dialogu bez nutnosti zvolit nějakou option), ale špatně jsem si rozvrhla čas na dělání semestrálky, a nestíhám to došperkovat, snad Vás hra bavila :D
# mohla jsem si víc vyhrát s podmínkami pro konec, podmínění výhry skrz dostatek věcí v inventáři a dostatek skillů je podle mě dobrý nápad, ale cítí to aktuálně trochu ploše. 
# kdyby byl důmyslnější děj, tahle feature by byla o dost zábavnější
# mohla do winning conditions zahrnout ještě nedostatek HP, aby to byla reálná hrozba, v tomhle kódu jsou HP spíš jen taková ozdoba